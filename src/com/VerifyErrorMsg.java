package com;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


public class VerifyErrorMsg {
	static WebDriver driver;
	static String baseURL="https://www.gmail.com";
	public static void main(String[] args) throws InterruptedException {
		driver=new FirefoxDriver();
		driver.manage().window().maximize();//to maximizing the window
		driver.get(baseURL);//To navigate the URL
		WebElement SignIn=driver.findElement(By.name("signIn"));
		SignIn.click();
		//first way
		WebElement errorMsg=driver.findElement(By.id("errormsg_0_Email"));
		if(errorMsg.getText().equalsIgnoreCase("Enter your  email address.")){
			System.out.println("Verified");
		}
		else{
			System.out.println("errorMsg fail");
		}
		//Second way
		WebElement errorMsg2=driver.findElement(By.tagName("body"));
		if(errorMsg2.getText().contains("Enter your email address.")){
			System.out.println("Verified");
		}
		else{
			System.out.println("errorMsg2 fail");
		}
		//Third way
		 String errorMsg3=driver.getPageSource();
		 if(errorMsg3.contains("Enter your email address.")){
				System.out.println("Verified");
			}
			else{
				System.out.println("errorMsg3 fail");
			}
		
		driver.close();
	}
	
	
	
}
