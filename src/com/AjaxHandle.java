package com;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AjaxHandle {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.navigate().to("http://the-internet.herokuapp.com/dynamic_loading/1");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement btnStart = driver.findElement(By.xpath(".//*[@id='start']/button"));
		btnStart.click();

		// Implicit Wait Doesn't work
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// Element is available in DOM but not visible
		WebElement finishTxt = driver.findElement(By.xpath(".//*[@id='finish']/h4"));
		if (!finishTxt.isDisplayed()) {
			System.out.println("Finish Element is not Enable.");
			// Try Without Explicit wait or webdriverwait.
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(finishTxt));
			System.out.println(finishTxt.getText());
		} else {

			System.out.println(finishTxt.isEnabled() + " Finish Element is Enable.");
		}

		driver.close();
	}
}