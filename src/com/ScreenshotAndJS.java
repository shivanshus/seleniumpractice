package com;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ScreenshotAndJS {
	WebDriver driver;
	String baseURL = "https://www.gmail.com";

	@BeforeMethod
	public void openBrowser() {
		driver = new FirefoxDriver();
		driver.get(baseURL);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@AfterMethod
	public void closeBrowser() {

		// driver.quit();
	}

	@Test(description = "taking screenshot")
	public void Screeshot() throws IOException {
		File file = new File("E:\\google.png");
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileHandler.copy(src, file);
		// simulate js
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.confirm('Hello Selenium')");
		File file1 = new File("E:\\googlealert.png");
		File src1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileHandler.copy(src1, file1);
		driver.switchTo().alert().accept();

	}
}
