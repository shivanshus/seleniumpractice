package com;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNG {
  @Test
  public void test() {
	  System.out.println("In Test");
	    
  }
  @Test
  public void test1(){
	  System.out.println("In Test1");
  }
  
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("In BMothod");

  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("In AMethod");

  }

  @BeforeClass
  public void beforeClass() {
	  System.out.println("In BClass");

  }

  @AfterClass
  public void afterClass() {
	  System.out.println("In AClass");

  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("In BTest");

  }

  @AfterTest
  public void afterTest() {
	  System.out.println("In ATest");

  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("In BSuite");

  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("In ASuite");

  }

}
