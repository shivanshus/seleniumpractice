package com;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/*
 * @Author=""
 * 
 * */

public class FrameSwitchAndJSAlerts {
	
	 WebDriver driver;
	 String baseURL="https://netbanking.hdfcbank.com/netbanking/";
	@BeforeMethod
	public void openBrowser(){
		driver=new FirefoxDriver();
		driver.get(baseURL);// navigate the address
		driver.manage().window().maximize();//for maximizing window
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	}
	@AfterMethod
	public void closeBrowser(){
		driver.quit();
	}
	//Priority--When to execute test
	@Test(priority=1)
	public void verifyMsg1() throws InterruptedException{
		//First way to switch on frame
		//driver.switchTo().frame("login_page");//first way
		//driver.switchTo().frame(1);//second way using index
		/*List<WebElement> framelist=driver.findElements(By.tagName("frame"));
		for(WebElement frame:framelist){
			if(frame.getAttribute("name").equalsIgnoreCase("login_page")){
				driver.switchTo().frame("login_page");
			}
			System.out.println(frame.getAttribute("name"));
		}*/
		driver.switchTo().frame(driver.findElement(By.name("login_page")));
		WebElement continuebtn=driver.findElement(By.xpath("//*[@alt='continue']"));
		continuebtn.click();
		//handling alerts
		Alert alt=driver.switchTo().alert();
		System.out.println(alt.getText());
		alt.accept();//pressing ok button
		/*JavascriptExecutor js=((JavascriptExecutor)driver);
		js.executeScript("window.confirm('Hi Vinay')");
		System.out.println(alt.getText());
		alt.dismiss();
		js.executeScript("window.prompt('Hi Vinay')");
		alt.sendKeys("Hello Selenium");
		Thread.sleep(2000);
		alt.accept();*/
		//alt.dismiss();//pressing cancel btn
		//alt.sendKeys("");//in case pf prompt
		
		
		
	}
}
