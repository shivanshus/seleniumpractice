package com;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class JavaScriptAlert {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("https://netbanking.hdfcbank.com/netbanking/");

		WebElement frame = driver.findElement(By.name("login_page"));
		driver.switchTo().frame(frame);// second way
		WebElement continuBtn = driver.findElement(By.xpath(".//table/tbody/tr[6]/td[2]/a/img"));
		continuBtn.click();

		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		alert.accept();

		driver.close();
	}

}
