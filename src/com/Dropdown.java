package com;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Dropdown {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.navigate().to("http://the-internet.herokuapp.com/dropdown");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		WebElement dropdown = driver.findElement(By.id("dropdown"));
		List<WebElement> options = dropdown.findElements(By.tagName("option"));

		// First Way to perform dropdown action
		for (WebElement element : options) {
			System.out.println(element.isEnabled());
			if (element.isEnabled()) {
				element.getText().equalsIgnoreCase("Option 1");
				element.click();
			} else {
				System.out.println("Option is Disabled to select");
			}
		}

		// Second way to perform
		Select sltOption = new Select(dropdown);
		// Choose any way from below
		sltOption.selectByVisibleText("Option 2");
		sltOption.selectByValue("1");
		sltOption.selectByIndex(2);

		driver.close();
	}

}
