package com;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ErrorMsg_TestNG {
	
	WebDriver driver;
	String baseURL="https://www.gmail.com";
	
	@BeforeMethod
	public void openBrowser(){
		driver=new FirefoxDriver();
		driver.get(baseURL);
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	@AfterMethod
	public void closeBrowser(){
		driver.quit();
	}
	
	@Test(description="First way to verify error msg")
	public void verifyErrorMsg(){
		WebElement signIn=driver.findElement(By.name("signIn"));
		signIn.click();
		WebElement msg=driver.findElement(By.id("errormsg_0_Email"));
		
		assert msg.getText().equalsIgnoreCase("Enter your email address."):"First way to verify error msg is fail";
		
		
	}
	
	@Test(description="Second way to verify error msg")
	public void verifyErrorMsg2(){
		WebElement signIn=driver.findElement(By.name("signIn"));
		signIn.click();
		WebElement msg=driver.findElement(By.tagName("body"));
		
		assert msg.getText().contains("Enter  your email address."):"Second way to verify error msg is fail";
		
		
	}
	@Test( description="Third way to verify error msg")
	public void verifyErrorMsg3(){
		WebElement signIn=driver.findElement(By.name("signIn"));
		signIn.click();
		String msg=driver.getPageSource();
		
		assert msg.contains("Enter your email address."):"Third way to verify error msg is fail";
		
		
	}
	
	
	
	
	

}
