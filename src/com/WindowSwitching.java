package com;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class WindowSwitching {

	WebDriver driver;
	String baseURL = "http://www.naukri.com/";

	@BeforeMethod
	public void openBrowser() {
		driver = new FirefoxDriver();
		driver.get(baseURL);// navigate the address
		driver.manage().window().maximize();// for maximizing window
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@AfterMethod
	public void closeBrowser() {
		driver.quit();
	}

	// Priority--When to execute test
	@Test(priority = 1)
	public void verifyMsg1() throws InterruptedException {

		WebElement JobAlert = driver.findElement(By.linkText("Create a new Job Alert"));
		JobAlert.sendKeys(Keys.ENTER);
		String parentWin = driver.getWindowHandle();
		Set<String> allWin = driver.getWindowHandles();
		String childwin = null;
		for (String child : allWin) {
			if (!child.equals(parentWin)) {
				childwin = child;
				driver.switchTo().window(child);
			}

		}
		if (driver.getTitle().equalsIgnoreCase("Create your Job Alerts, Get matching jobs in your inbox")) {
			System.out.println("Successfully switched");
			driver.switchTo().window(parentWin);
			if (driver.getTitle().equalsIgnoreCase(
					"Naukri.com � Jobs � Jobs in India � Recruitment � Job Search � Employment � Job Vacancies")) {
				System.out.println("Successfully switched on parent win");
				driver.close();
			}
		} else {
			System.out.println("Not able to switched");
		}
	}
}
