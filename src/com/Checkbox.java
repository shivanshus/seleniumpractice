package com;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Checkbox {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("http://the-internet.herokuapp.com/checkboxes");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		WebElement checkbox = driver.findElement(By.id("checkboxes"));
		List<WebElement> inputCheckboxs = checkbox.findElements(By.tagName("input"));

		for (WebElement box : inputCheckboxs) {
			if (!box.isSelected()) {
				System.out.println("is not selected");
				box.click();
			} else {
				System.out.println("is selected");
			}
		}
		driver.close();
	}
}
