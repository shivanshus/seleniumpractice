package com;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class VerifyErrMsg_TeastNG {
	static WebDriver driver;
	static String baseURL="https://www.gmail.com";
	
	@BeforeMethod
	public void openBrowser(){
		driver=new FirefoxDriver();
		driver.manage().window().maximize();//to maximizing the window
		driver.get(baseURL);//To navigate the URL
	}
	@AfterMethod
	public void closeBrowser(){
		driver.close();
	}
	@Test
	public void verifyErrMsg1(){
		WebElement SignIn=driver.findElement(By.name("signIn"));
		SignIn.click();
		//first way
		WebElement errorMsg=driver.findElement(By.id("errormsg_0_Email"));
		assert errorMsg.getText().equalsIgnoreCase("Enter your  email address."):"errorMsg1 is fail";
		
	}
	@Test(priority=1)
	public void verifyErrMsg2(){
		//Second way
		WebElement SignIn=driver.findElement(By.name("signIn"));
		SignIn.click();
		WebElement errorMsg2=driver.findElement(By.tagName("body"));
		assert errorMsg2.getText().contains("Enter your email address."):"errorMsg2 is fail";
		}
	 @Test(priority=2)
	 public void verifyErrMsg3(){
		//Third way
		 WebElement SignIn=driver.findElement(By.name("signIn"));
			SignIn.click();
		 String errorMsg3=driver.getPageSource();
		 assert errorMsg3.contains("Enter your email address."):"errorMsg3 is fail";
			}
		
	}

