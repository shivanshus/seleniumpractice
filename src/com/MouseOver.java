package com;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
public class MouseOver {
	
	static WebDriver driver;
	static String baseURL="http://www.naukri.com/";
	public static void main(String[] args) {
		//System.setProperty("webdriver.chrome.driver","C:\\Users\\PC-2\\Downloads\\chromedriver.exe");
		//System.setProperty("webdriver.ie.driver","C:\\Users\\PC-2\\Downloads\\IEDriverServer.exe");

		driver=new FirefoxDriver();
		
		driver.get(baseURL);// navigate the address
		driver.manage().window().maximize();//for maximizing window
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		WebElement EntranceExam=driver.findElement(By.xpath("//*[@id='mNav']/div[1]/ul/li[6]/a"));
		WebElement IITJEE=driver.findElement(By.xpath("//*[@id='resServ']/ul[1]/li[1]/div/a[1]"));
       //Mouse over using action class
		Actions act=new Actions(driver);
	    act.moveToElement(EntranceExam).click().build().perform();
	    act.click(IITJEE).build().perform();
		driver.close();//Close browser window
		
		
	}
	
	
	

}
