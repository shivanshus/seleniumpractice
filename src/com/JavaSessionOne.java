package com;

public class JavaSessionOne {

	public static void main(String[] args) {

		System.out.print("Selenium");
		System.out.println("Hello Selenium");
		System.out.print("java");

		// Data types
		int i = 10000;// 32 bit
		System.out.println(i);
		long l = 2121;// 64 bit
		float f = 12.3f;
		double d = 32.323;
		char c = 'a';
		String str = "Hello Java";

		boolean bool = true;

		// Concatenation

		int a = 10;
		int b = 20;
		String str1 = "Hello";
		String str2 = "Java";

		System.out.println(a + b + str1 + str2);
		System.out.println(str1 + str2 + (a + b));

		// Conditions

		int x = 100;
		int y = 500;
		int z = 300;

		if (x > y && x > z) {
			System.out.println("X is the greatest no and value is: " + x);
		} else if (y > z) {
			System.out.println("Y is the greatest no and value is: " + y);
		} else {
			System.out.println("Z is the greatest no and value is: " + z);

		}

		// Loops
		int w = 10;
		while (w > 0) {
			if (w == 5) {
				System.out.println("value of W :" + w);
				break;
			}
			w--;
			// w=w+1;
		}

		// for loops
		for (int s = 0; s < 10; s++) {
			System.out.println("value of s:" + s);
		}

	}

}
