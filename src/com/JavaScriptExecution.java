package com;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JavaScriptExecution {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
		driver = new FirefoxDriver();

		JavascriptExecutor javaScriptExec = ((JavascriptExecutor) driver);
		javaScriptExec.executeScript("window.confirm('JS Alert')");
		// Explicit wait for alert to be present.
		WebDriverWait wait = new WebDriverWait(driver, 10);
		if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
			System.out.println(driver.switchTo().alert().getText());
			driver.switchTo().alert().accept();
		}
	}
}
