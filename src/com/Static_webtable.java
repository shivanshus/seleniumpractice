package com;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Static_webtable {
	
	static WebDriver driver;
	static String baseURL="http://www.w3schools.com/html/html_tables.asp";
	public static void main(String[] args) throws InterruptedException {
		driver=new FirefoxDriver();
		driver.manage().window().maximize();//to maximizing the window
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get(baseURL);//To navigate the URL
	
		WebElement table=driver.findElement(By.xpath(".//*[@id='main']/table[1]"));	
		List<WebElement> tr=table.findElements(By.tagName("tr"));
		for(WebElement trow:tr){
		System.out.println("|"+trow.getText()+ "|");
		}
		driver.close();
		
		
	}

}
