package com;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseHover {

	public static void main(String[] args) {

		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");

		driver = new FirefoxDriver();
		driver.get("http://the-internet.herokuapp.com/hovers");

		List<WebElement> users = driver.findElements(By.className("figure"));
		Actions actions = new Actions(driver);

		for (WebElement user : users) {
			// Best way to perform action use Actions class and perfom build and
			// perform action on each element.Your getText() method wont return
			// anything if you build and perform at the end.
			actions.moveToElement(user).build().perform();
			System.out.println(user.findElement(By.className("figcaption")).getText());

		}
		// Doesn't provide getText method result.
		// actions.build().perform();
		driver.close();
	}

}
