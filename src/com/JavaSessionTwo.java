package com;

import java.util.*;

public class JavaSessionTwo {
	
	public static void main(String[] args) {
		//One dim array
		int[] in=new int[5];
		try{
		in[0]=10;
		in[1]=20;
		in[2]=30;
		in[3]=40;
		in[4]=50;
		in[5]=60;
		}
		catch(Exception e){
		//	e.printStackTrace();
		}
		for(int a=0;a<in.length;a++){
		System.out.println(in[a]);
		}
		                     //row[]cols[]
		int [][] twoDim=new int[3][3];
		//First row
		twoDim[0][0]=10;
		twoDim[0][1]=20;
		twoDim[0][2]=30;
		
		//Second row
		twoDim[1][0]=100;
		twoDim[1][1]=200;
		twoDim[1][2]=300;
		//third row
		twoDim[2][0]=60;
		twoDim[2][1]=80;
		twoDim[2][2]=90;
		
		System.out.println("Rows size: "+twoDim.length);
		System.out.println("cols size: "+twoDim[0].length);
 
		for(int r=0;r<twoDim.length;r++){
			for(int c=0;c<twoDim[r].length;c++){
				System.out.print(twoDim[r][c]+"\t");
			}
			System.out.println();
		}
		
		//for Each
		for(int[] data:twoDim){
			for(int value:data){
				System.out.println(value);
			}
		}
		
		Object[] obj=new Object[3];
		obj[0]=10;
		obj[1]="String";
		obj[2]=true;
		
		//ArrayList
		
		ArrayList<String> arrlist=new ArrayList<String>();
		arrlist.add("This");
		arrlist.add("is");
		arrlist.add("a");
		arrlist.add("String");
		arrlist.add("Array");
		
		System.out.println(arrlist);
		
		for(String str:arrlist){
			System.out.println(str);
		}
		
		//Sets
		
		Set<String> dups=new HashSet<String>();
		dups.add("Lalit");
		dups.add("Nancy");
		dups.add("Nishank");
		dups.add("Lalit");
		
		System.out.println(dups.size());
		for(Object value:dups){
			System.out.println(value);
		}
		
		//Hashmap
		
		HashMap<Integer,String> maps=new HashMap<Integer, String>();
		maps.put(1,"Mohita");
		maps.put(2,"Ashok");
		maps.put(3,"Kamal");
		
		System.out.println(maps.get(2));
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	
	

}
