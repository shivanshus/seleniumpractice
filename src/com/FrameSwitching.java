package com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FrameSwitching {

	public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");

		driver = new FirefoxDriver();
		driver.get("https://netbanking.hdfcbank.com/netbanking/");

		driver.switchTo().frame("login_page");// first way
		// driver.switchTo().frame(1);// second way using index

		WebElement userID = driver.findElement(By.className("input_password"));
		userID.sendKeys("TEst123");

		driver.close();
	}

}
