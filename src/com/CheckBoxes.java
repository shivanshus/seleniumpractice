package com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class CheckBoxes {
	static WebDriver driver;
	static String baseURL="https://www.gmail.com";
	public static void main(String[] args) throws InterruptedException {
		driver=new FirefoxDriver();
		driver.manage().window().maximize();//to maximizing the window
		driver.get(baseURL);//To navigate the URL
		WebElement CheckBox=driver.findElement(By.id("PersistentCookie"));
		if(!CheckBox.isSelected()){
			System.out.println("Unchecked");
		}
		else{
			CheckBox.click();
		}
		
		//driver.close();
	}
	
	
	
}
