package com;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AjaxAndWebTable {
	static WebDriver driver;
	static String baseURL="https://www.google.com";
	public static void main(String[] args) throws InterruptedException {
		driver=new FirefoxDriver();
		driver.manage().window().maximize();//to maximizing the window
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get(baseURL);//To navigate the URL
		//driver.findElement(By.name("q"));//Finding the webelements
		WebElement SearchBox=driver.findElement(By.name("q"));
		SearchBox.sendKeys("Selenium");//Typing in the text box
		//Explicit wait
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("gssb_m")));
		WebElement table=driver.findElement(By.className("gssb_m"));
		List<WebElement> tr=table.findElements(By.tagName("tr"));
		for(WebElement trow:tr){
			List<WebElement> td=trow.findElements(By.tagName("td"));
			for(WebElement tdata:td){
				System.out.println(tdata.getText());
			}
			//To get the text of the element
		}
		driver.close();
		
		
	}
	
	
	
}
