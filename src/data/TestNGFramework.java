package data;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * ------------------------------------
 * Run As TestNG
 * Following is the sequence 
 * Annotation will be called
 * No matter which method called first 
 * -------------------------------------
 * Before Suite Method Called
 * Before Test Method Called 
 * Before Class Method Called 
 * Before Method Called 
 * Test Method Called 
 * After Method Called 
 * Before Method Called 
 * Test1 Method Called 
 * After Method Called 
 * After Class Method Called 
 * After Test Method Called 
 * PASSED: test 
 * PASSED: test1
 */

public class TestNGFramework {


	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Before Suite Method Called");

	}
	
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before Test Method Called");
		/*WebDriver driver;
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("http://the-internet.herokuapp.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.close();*/

	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("Before Class Method Called");

	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Before Method Called");

	}

	@Test(dependsOnGroups="hello")
	public void test() {
		System.out.println("Test Method Called");

	}

	@Test(groups="hello")
	public void test1() {
		System.out.println("Test1 Method Called");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method Called");

	}

	@AfterClass
	public void afterClass() {
		System.out.println("After Class Method Called");

	}

	@AfterTest
	public void afterTest() {
		System.out.println("After Test Method Called");

	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite Method Called");

	}
}
