package data;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * 
 * OUTPUT 
 * NAME : JAVA | VALUE : .NET 
 * NAME : PHP | VALUE : JAVASCRIPT 
 * NAME : PHYTHON | VALUE : ANGULAR 
 * PASSED: detail("JAVA", ".NET") 
 * PASSED: detail("PHP", "JAVASCRIPT") 
 * PASSED: detail("PHYTHON", "ANGULAR")
 *
 */
public class DataProviderWithTestNG {

	@DataProvider
	public Object[][] getName() {
		String obj1 = "JAVA";
		String value1 = ".NET";
		String obj2 = "PHP";
		String value2 = "JAVASCRIPT";
		String obj3 = "PHYTHON";
		String value3 = "ANGULAR";

		return new Object[][] { { obj1, value1 }, { obj2, value2 }, { obj3, value3 } };
	}

	@Test(dataProvider = "getName")
	public void detail(String name, String value) {
		System.out.println("NAME : " + name + " | VALUE : " + value);
	}
}
