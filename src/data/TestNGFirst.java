package data;

import org.testng.annotations.Test;

public class TestNGFirst {
	String message;

	@Test(priority = 1, dependsOnMethods = { "test3" })
	public void test1() {
		message = "hi";
		System.out.println("int test1" + message);
	}

	@Test(priority = 0)
	public void test2() {
		message = "hello";
		System.out.println("int test2" + message);
	}

	@Test(priority = 3)
	public void test3() {
	}
}
