package data;

import org.testng.annotations.Test;

public class TestNGSecond {

	@Test(priority = 1)
	public void secondtest1() {
	}

	@Test(priority = 2,dependsOnMethods = { "data.TestNGFirst.test1" })
	public void secondtest2() {
		System.out.println("second2 test");
	}

	@Test(priority = 3)
	public void secondtest3() {
	}

	@Test
	public void test234(){
		
	}
}
